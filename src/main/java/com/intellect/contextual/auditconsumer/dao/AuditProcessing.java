package com.intellect.contextual.auditconsumer.dao;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.intellect.contextual.auditconsumer.constant.QueryConstant;
import com.intellect.contextual.auditconsumer.model.AuditContext;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class AuditProcessing {

  @Autowired private Gson gson;
  @Autowired private ContextualAuditDao dao;

  /**
   * This function is used for saving upStreaAudit Req & Res
   *
   * @param auditContext
   */
  public void saveUpstreamAudit(@NonNull AuditContext auditContext) {
    String tableName = QueryConstant.ICP_UPSTREAM_AUDIT;
    Map<String, Object> auditUpstream = new HashMap<>();
    String payLoad = auditContext.getPayload() != null ? auditContext.getPayload() : "";
    payLoad = payLoad.replaceAll("\n", " ").trim();
    auditUpstream.put(QueryConstant.API_NAME, auditContext.getApiType());
    auditUpstream.put(QueryConstant.API_CP_REFERENCE_ID, auditContext.getCpReferenceId());
    auditUpstream.put(QueryConstant.API_CHANNEL_REQUEST_ID, auditContext.getChannelRequestId());
    auditUpstream.put(
        QueryConstant.API_ORIG_CHANNEL_REQUEST_ID, auditContext.getOrginalRequestId());
    auditUpstream.put(QueryConstant.API_DATE_TIME, auditContext.getDateTime());
    auditUpstream.put(QueryConstant.API_SYSTEM_CODE, auditContext.getSystemCode());
    auditUpstream.put(QueryConstant.API_REQ_RESP, auditContext.getReqRes());
    auditUpstream.put(QueryConstant.API_URI, auditContext.getApiUri());
    auditUpstream.put(QueryConstant.API_HEADER, auditContext.getApiUri());
    auditUpstream.put(QueryConstant.API_BODY, payLoad.substring(0, Math.min(payLoad.length(), 3999)));
    auditUpstream.put(QueryConstant.ALU_INSERTED_TIME, new Date());

    dao.insert(tableName, auditUpstream);

    if (auditContext.getApiType().equals(QueryConstant.GET_RECOMMENDATION)
        && auditContext.getReqRes().equals(QueryConstant.RES)) {
      insertRecmAudit(auditContext);
    }
  }

  /**
   * This function is used for saving recommAudit
   *
   * @param auditContext
   */
  public void insertRecmAudit(@NonNull AuditContext auditContext) {
    String tableName = QueryConstant.ICP_RECOMMENDATION_AUDIT;
    String payLoad = auditContext.getPayload() != null ? auditContext.getPayload() : "";
    payLoad = payLoad.replaceAll("\n", " ").trim();
    Map<String, Object> auditUpstream = new HashMap<>();
    auditUpstream.put(QueryConstant.API_GR_CP_REFERENCE_ID, auditContext.getCpReferenceId());
    auditUpstream.put(QueryConstant.API_GR_CHANNEL_REQUEST_ID, auditContext.getChannelRequestId());
    auditUpstream.put(QueryConstant.API_DATE_TIME, auditContext.getDateTime());
    auditUpstream.put(QueryConstant.API_BODY, payLoad.substring(0, Math.min(payLoad.length(), 3999)));
    auditUpstream.put(QueryConstant.API_CP_RECOMMENDATION, auditContext.getOfferingCode());
    auditUpstream.put(QueryConstant.ALU_INSERTED_TIME, new Date());
    log.debug("insertRecmAudit : {}", auditUpstream);
    dao.insert(tableName, auditUpstream);
  }

  /** This function is used for updating recommAudi */
  public void updateRecmAudit(@NonNull Map<String, String> upStreamMap) {
    String tableName = QueryConstant.ICP_RECOMMENDATION_AUDIT;
    Map<String, Object> whereClause = new HashMap<>();
    Map<String, Object> setClause = new HashMap<>();

    setClause.put(QueryConstant.API_USER_SELECTION, upStreamMap.get(QueryConstant.OFFERING_CODE));
    setClause.put(
        QueryConstant.API_SR_CHANNEL_REQUEST_ID, upStreamMap.get(QueryConstant.REQUEST_ID));

    whereClause.put(
        QueryConstant.API_GR_CHANNEL_REQUEST_ID,
        upStreamMap.get(QueryConstant.ORIGINAL_CHANNEL_REQUEST_ID));
    log.debug("updateRecmAudit : {}", upStreamMap);
    dao.update(tableName, setClause, whereClause);
  }

  public void saveRecommendationAudit(String requestBody) {
    Map<String, Map<String, String>> requestMap;
    Map<String, String> upStreamMap = new HashMap<>();
    requestMap =
        gson.fromJson(requestBody, new TypeToken<Map<String, Map<String, String>>>() {}.getType());

    requestMap.forEach((key, value) -> upStreamMap.putAll(value));
    //    for (Map.Entry<String, LinkedHashMap<String, String>> entrydata : requestMap.entrySet()) {
    //      for (Map.Entry<String, String> nameEntry : entrydata.getValue().entrySet()) {
    //        String name = nameEntry.getKey();
    //        String value = nameEntry.getValue();
    //        upStreamMap.put(name, value);
    //      }
    //    }
    updateRecmAudit(upStreamMap);
  }
  /**
   * Save audit internal.
   *
   * @param auditContext the audit context
   */
  public void saveAuditInternal(@NonNull AuditContext auditContext) {
    String tableName = QueryConstant.ICP_PROCESSING_AUDIT;
    Map<String, Object> auditProcessMap = new HashMap<>();
    auditProcessMap.put(QueryConstant.API_CP_REFERENCE_ID, auditContext.getCpReferenceId());
    auditProcessMap.put(QueryConstant.API_CHANNEL_REQUEST_ID, auditContext.getChannelRequestId());
    auditProcessMap.put(QueryConstant.API_DATE_TIME, auditContext.getDateTime());
    auditProcessMap.put(QueryConstant.API_VALUE, auditContext.getApiValue());
    auditProcessMap.put(QueryConstant.API_LAYER, auditContext.getLayer());
    auditProcessMap.put(QueryConstant.API_STAGE, auditContext.getStage());
    auditProcessMap.put(QueryConstant.API_FINE_GRAIN, auditContext.getFineGrain());
    auditProcessMap.put(QueryConstant.API_OFFERING_CODE, auditContext.getOfferingCode());
    auditProcessMap.put(QueryConstant.API_DECISION_PARAM, auditContext.getDecisionParam());
    auditProcessMap.put(QueryConstant.API_FIELD_CODE, auditContext.getFieldCode());
    auditProcessMap.put(QueryConstant.API_VRULE_CODE, auditContext.getVRuleCode());
    auditProcessMap.put(QueryConstant.API_ERULE_CODE, auditContext.getERuleCode());
    auditProcessMap.put(QueryConstant.API_PAYLOAD, auditContext.getPayload());
    auditProcessMap.put(QueryConstant.API_STATUS, auditContext.getStatus());
    auditProcessMap.put(QueryConstant.ALU_INSERTED_TIME, new Date());
    //   System.out.println("====================Save audit internal.");
    log.debug("auditProcessMap : {}", auditProcessMap);
    dao.insert(tableName, auditProcessMap);
    log.debug("====================Save audit internal.");
  }
}
